package edu.msu.komesuel.btwifichat2;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

public class Messenger {

    /**
     * Connection States
     */
    static final int NOT_CONNECTED = 0;
    static final int STATE_LISTENING = 1;
    static final int STATE_CONNECTING = 2;
    static final int STATE_CONNECTED_BT = 3;
    static final int STATE_CONNECTED_WF = 4;
    static final int STATE_CONNECTION_FAILED = 5;
    static final int STATE_MESSAGE_RECEIVED = 6;
    static final int STATE_IMAGE_RECEIVED = 7;
    static final int STATE_ADDRESS_RECEIVED = 8;
    static final int STATE_SERVER_STARTED = 9;
    static final int STATE_INIT_RECEIVED = 10;

    private int btState;
    private int wfState;

    private String messageType;

    /*
    private final String connectionType;

    private final Handler messageHandler;
    */

    private final Handler handler;

    private final BluetoothAdapter bluetoothAdapter;


    private BServerClass bServerClass;
    private BClientClass bClientClass;
    private BSendReceive bSendReceive;

    private ThreadHandler threadHandler;
    //private WServerClass wServerClass;
    private WClientClass wClientClass;
    //private WSendReceive wSendReceive;

    private String senderAdd;

    //public static Queue<>


    /**
     * Bluetooth
     */
    private static final String APP_NAME = "BT Chat";
    private static final UUID myUUID = UUID.fromString("ec9153de-86fb-11e9-bc42-526af7764f64");

    public Messenger(Handler h) {
        handler = h;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        btState = NOT_CONNECTED;
        wfState = NOT_CONNECTED;
        //Log.i("peers", "messenger");
    }

    public int getBtState() {
        Log.i("peers", "bt state: "+btState);
        return btState;
    }

    public int getWfState() {
        Log.i("peers", "wf state: "+wfState);
        return wfState;
    }

    public void changeWfState(int state) {
        Log.i("peers", "changeWfState: "+state);
        wfState = state;
        Message message = Message.obtain();
        message.what = wfState;
        handler.sendMessage(message);
    }

    /**
     * Bluetooth Server Class
     */
    public class BServerClass extends Thread {
        private BluetoothServerSocket btServerSocket;
        private BluetoothSocket btSocket = null;

        private Message message;

        public BServerClass() {
            try {
                btServerSocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord(APP_NAME, myUUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            while (btSocket == null) {
                try {
                    btSocket = btServerSocket.accept();
                    btState = STATE_CONNECTING;
                } catch (IOException e) {
                    e.printStackTrace();
                    btState = STATE_CONNECTION_FAILED;
                }
                message = Message.obtain();
                message.what = btState;
                handler.sendMessage(message);
                if (btSocket != null) {
                    btState = STATE_CONNECTED_BT;
                    bSendReceive = new BSendReceive(btSocket);
                    bSendReceive.start();
                    break;
                }
            }
            message = Message.obtain();
            message.what = btState;
            handler.sendMessage(message);
        }

        public void cancel() {
            try {
                Log.e("peers", "close() socket success");
                btServerSocket.close();
                btSocket.close();
            } catch (IOException e) {
                Log.e("peers", "close() socket failed", e);
            } catch (NullPointerException d) {
                Log.e("peers", "close() socket failed", d);
            }
        }
    }

    public void startBtServer() {
        bServerClass = new BServerClass();
        bServerClass.start();
    }

    /**
     * Bluetooth Client Class
     */
    public class BClientClass extends Thread {
        BluetoothDevice bluetoothDevice;
        BluetoothSocket socket;
        Message message;

        public BClientClass(BluetoothDevice device) {
            Log.i("peers","BT Client Class");
            bluetoothDevice = device;
            try {
                socket = device.createRfcommSocketToServiceRecord(myUUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {
            try {
                socket.connect();
                btState = STATE_CONNECTED_BT;
                message = Message.obtain();
                message.what = STATE_CONNECTED_BT;
                handler.sendMessage(message);

                bSendReceive = new BSendReceive(socket);
                bSendReceive.start();

            } catch (IOException e) {
                e.printStackTrace();
                btState = STATE_CONNECTION_FAILED;
                message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);

            }
        }

        public void cancel() {
            try {
                Log.e("peers", "close() socket success");
                socket.close();
            } catch (IOException e) {
                Log.e("peers", "close() socket failed", e);
            } catch (NullPointerException d) {
                Log.e("peers", "close() socket failed", d);
            }
        }
    }

    public void startBtClient(BluetoothDevice device) {
        bClientClass = new BClientClass(device);
        bClientClass.start();
    }

    /**
     * WiFi Direct ThreadHandler
     */
    public class ThreadHandler extends Thread {
        private ServerSocket serverSocket;
        private Message message;
        private ArrayList<WServerClass> serverThreads = new ArrayList<>();
        private HashMap<String, WServerClass> serverMap = new HashMap<>();
        public void run() {
            while (serverSocket == null) {
                Log.i("peers", "threadhandler init");
                try {
                    serverSocket = new ServerSocket(8888);
                    message = Message.obtain();
                    message.what = STATE_SERVER_STARTED;
                    handler.sendMessage(message);
                    //Log.i("peers", "threadhandler run");
                } catch (IOException e) {
                    e.printStackTrace();
                    this.cancel();
                    break;
                }
            }
        }

        public void addThread(WServerClass serverClass, String mId) {
            serverMap.put(mId, serverClass);
            serverThreads.add(serverClass);
            Log.i("peers", "serverThreads size: "+serverThreads.size());

        }

        public void cancel() {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException d) {
                d.printStackTrace();
            }
            for (WServerClass serverClass : serverThreads) {
                serverClass.cancel();
            }
        }

    }

    public void startThreadHandler() {
        threadHandler = new ThreadHandler();
        threadHandler.start();
    }

    public void writeOnAllThreads(byte[] msg) {
        Log.i("peers", "serverThreads size write: "+threadHandler.serverThreads.size());

        for (WServerClass val : threadHandler.serverMap.values()) {
            val.write(msg);
        }

        //threadHandler.serverThreads.get(0).write(msg);
        //threadHandler.serverThreads.get(1).write(msg);
    }

    public void writeOnClientThreads(byte[] msg) {
        //Log.i("peers", "serverThreads size write: "+threadHandler.serverThreads.size());

        for (String key : threadHandler.serverMap.keySet()) {
            //Log.i("peers", "send all key: "+key);
            if (!key.equals(senderAdd)) {
                threadHandler.serverMap.get(key).write(msg);
            }
        }
        //threadHandler.serverThreads.get(0).write(msg);
        //threadHandler.serverThreads.get(1).write(msg);
    }

    public void simultaneousConnectionCase(Queue<String> requests) {

    }



    public void writeOnAllThreadsImg(byte[] msg) {
        //Log.i("peers", "serverThreads size write: "+threadHandler.serverThreads.size());

        for (WServerClass val : threadHandler.serverMap.values()) {
            val.writeImg(msg);
        }

        //threadHandler.serverThreads.get(0).write(msg);
        //threadHandler.serverThreads.get(1).write(msg);
    }

    public void writeOnAllClientsImg(byte[] msg) {
        //Log.i("peers", "serverThreads size write: "+threadHandler.serverThreads.size());

        for (String key : threadHandler.serverMap.keySet()) {
            //Log.i("peers", "send all key: "+key);
            if (!key.equals(senderAdd)) {
                threadHandler.serverMap.get(key).writeImg(msg);
            }
        }

        //threadHandler.serverThreads.get(0).write(msg);
        //threadHandler.serverThreads.get(1).write(msg);
    }

    /**
     * WiFi Direct Server Class
     */
    public class WServerClass extends Thread {
        //private ServerSocket serverSocket;
        private Socket socket = null;
        private Message message;
        private String id;
        WSendReceive wSendReceive;

        public WServerClass(){
            Log.i("peers", "wifi server init");
            /*
            try {
            serverSocket = new ServerSocket(8888);
            message = Message.obtain();
            message.what = STATE_SERVER_STARTED;
            handler.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
                this.cancel();
            }
            */
        }

        @Override
        public void run() {
            while (socket == null) {
                try {
                    socket = threadHandler.serverSocket.accept();
                    Log.i("peers", "server class run .accept");
                    wfState = STATE_CONNECTING;
                    message = Message.obtain();
                    message.what = wfState;
                    handler.sendMessage(message);
                } catch (IOException e) {
                    e.printStackTrace();
                    wfState = STATE_CONNECTION_FAILED;
                    Log.i("peers", "CONNECTION_FAILED server socket");
                    this.cancel();
                    break;
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    wfState = NOT_CONNECTED;
                    //this.cancel();
                    break;
                }
                if (socket != null) {
                    id = socket.getRemoteSocketAddress().toString();
                    Log.i("peers","socket id: "+id);
                    wfState = STATE_CONNECTED_WF;
                    wSendReceive = new WSendReceive(socket);
                    wSendReceive.start();
                    threadHandler.addThread(this, id);
                    break;
                }
            }

            message = Message.obtain();
            message.what = wfState;
            handler.sendMessage(message);
        }

        public void write(byte[] msg) {
            //Log.i("peers", "server class write");
            try {
                wSendReceive.write(msg);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        public void writeImg(byte[] img) {
            //Log.i("peers","server class write img");
            wSendReceive.writeImage(img);
        }

        public void cancel() {
            try { if (threadHandler.serverSocket!=null) {threadHandler.serverSocket.close();}
            } catch (IOException e) {
                Log.e("peers", "close() socket failed", e);
            } catch (NullPointerException d) {
                Log.e("peers", "close() socket failed", d);
            }

            try {
                if (socket!=null) {
                    socket.close();
                    wfState = NOT_CONNECTED;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    public void startWfServer() {
        WServerClass wServerClass = new WServerClass();
        wServerClass.start();
    }

    /**
     * WiFi Direct Client Class
     */
    public class WClientClass extends Thread {

        Socket socket;
        String hostAdd;
        Message message;
        WSendReceive wSendReceive;

        public WClientClass(InetAddress hostAddress) {
            Log.i("peers", "wifi client init");
            hostAdd = hostAddress.getHostAddress();
            socket = new Socket();
        }

        @Override
        public void run() {
            try {
                socket.connect(new InetSocketAddress(hostAdd, 8888), 500);
                wfState = STATE_CONNECTED_WF;
                message = Message.obtain();
                message.what = STATE_CONNECTED_WF;
                handler.sendMessage(message);
                wSendReceive = new WSendReceive(socket);
                wSendReceive.start();
            } catch (IOException e) {
                Log.i("peers", "CONNECTION_FAILED client");
                wfState = STATE_CONNECTION_FAILED;
                message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);
                e.printStackTrace();
                this.cancel();
            }
        }

        public void cancel() {
            try {
                Log.e("peers", "close() socket success");
                socket.close();
                wfState = NOT_CONNECTED;
            } catch (IOException e) {
                Log.e("peers", "close() socket failed", e);
            } catch (NullPointerException d) {
                Log.e("peers", "close() socket failed", d);
            }
        }
    }

    public void startWfClient(InetAddress add) {
        wClientClass = new WClientClass(add);
        wClientClass.start();
    }

    /**
     * Bluetooth SendReceive Class
     */
    private class BSendReceive extends Thread {
        private final BluetoothSocket bluetoothSocket;
        private InputStream inputStream;
        private OutputStream outputStream;
        private DataInputStream dataInputStream;
        private DataOutputStream dataOutputStream;
        private Message message;

        public BSendReceive(BluetoothSocket skt) {

            bluetoothSocket = skt;
            try {
                inputStream = bluetoothSocket.getInputStream();
                outputStream = bluetoothSocket.getOutputStream();
                dataInputStream = new DataInputStream(inputStream);
                dataOutputStream = new DataOutputStream(outputStream);
            } catch (IOException e) {
                message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);
                e.printStackTrace();
            }

        }

        @Override
        public void run() {
            byte[] data;
            int len;
            int bytes;
            boolean flag = true;
            String messageType = "";

            while (bluetoothSocket != null) {
                if (flag) {
                    byte[] buffer = new byte[1024];
                    try {
                        bytes = inputStream.read(buffer, 0, 3);
                        messageType = new String(buffer, 0, bytes);
                        //Log.i("peers", "read message: "+messageType);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    flag = false;
                } else {
                    if (messageType.equals("str")) {
                        Log.i("peers", "message type: String");
                        data = new byte[1024];
                        try {
                            bytes = inputStream.read(data);
                            //Log.i("peers", "bytes message: "+bytes);
                            if (bytes>0) {
                                handler.obtainMessage(STATE_MESSAGE_RECEIVED,bytes,-1,data).sendToTarget();
                            } else {
                                break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    } else if (messageType.equals("img")) {
                        try {
                            len = dataInputStream.readInt();
                            Log.i("peers", "about to read image: "+len);
                            data = new byte[len];
                            if (len > 0) {
                                //Log.i("peers", "if len > 0");
                                dataInputStream.readFully(data,0,data.length);
                                handler.obtainMessage(STATE_IMAGE_RECEIVED,-1,-1,data).sendToTarget();
                            } else {
                                data = new byte[1024];
                                bytes = inputStream.read(data);
                                handler.obtainMessage(STATE_MESSAGE_RECEIVED,bytes,-1,data).sendToTarget();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                    flag = true;
                }
            }

        }

        public void write(byte[] bytes) {
            try {
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void writeImage(byte[] bytes) {
            try {
                dataOutputStream.writeInt(bytes.length);
                dataOutputStream.write(bytes, 0, bytes.length);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void cancel() {
            try { if (bluetoothSocket != null) bluetoothSocket.close(); } catch (IOException e) {}
            try { if (inputStream != null) inputStream.close(); } catch(IOException e) {}
            try { if (dataInputStream != null) dataInputStream.close(); } catch(IOException e) {}
            try { if (outputStream != null) outputStream.close(); } catch(IOException e) {}
            try { if (dataOutputStream != null) dataOutputStream.close(); } catch(IOException e) {}
        }
    }

    /**
     * WiFi Direct Send Receive Class
     */
    private class WSendReceive extends Thread {
        private Socket socket;
        private InputStream inputStream;
        private OutputStream outputStream;
        private DataInputStream dataInputStream;
        private DataOutputStream dataOutputStream;
        private Message message;
        boolean isClosed = false;

        public WSendReceive(Socket skt) {
            socket = skt;
            try {
                inputStream = socket.getInputStream();
                outputStream = socket.getOutputStream();
                dataInputStream = new DataInputStream(inputStream);
                dataOutputStream = new DataOutputStream(outputStream);

            } catch (IOException e) {
                wfState = STATE_CONNECTION_FAILED;
                message = Message.obtain();
                message.what = wfState;
                handler.sendMessage(message);
                e.printStackTrace();
                this.cancel();
            }

        }

        @Override
        public void run() {
            int bytes;
            byte[] data;
            int len;
            boolean flag = true;

            while (socket != null && !socket.isClosed()) {
                if (flag) {
                    byte[] buffer = new byte[1024];
                    try {
                        senderAdd = socket.getRemoteSocketAddress().toString();
                        Log.i("peers", "sender address: "+senderAdd);
                        bytes = inputStream.read(buffer, 0, 3);
                        if (bytes>0) {
                            messageType = new String(buffer, 0, bytes);


                        } else {
                            break;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        this.cancel();
                        wfState = STATE_CONNECTION_FAILED;
                        message = Message.obtain();
                        message.what = STATE_CONNECTION_FAILED;
                        handler.sendMessage(message);
                        break;
                    }
                    flag = false;
                } else {
                    if (messageType!=null && messageType.equals("str")) {
                        Log.i("peers", "message type: String");
                        data = new byte[1024];
                        try {
                            bytes = inputStream.read(data);

                            if (bytes>0) {
                                handler.obtainMessage(STATE_MESSAGE_RECEIVED,bytes,-1,data).sendToTarget();
                            } else {
                                break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    } else if (messageType!=null && messageType.equals("img")) {
                        try {
                            Log.i("peers", "about to read image: ");
                            len = dataInputStream.readInt();

                            data = new byte[len];
                            if (len > 0) {
                                dataInputStream.readFully(data,0,data.length);
                                handler.obtainMessage(STATE_IMAGE_RECEIVED,-1,-1,data).sendToTarget();
                            } else {
                                break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    } else if (messageType!=null && messageType.equals("add")) {
                        Log.i("peers", "message type: Address");
                        data = new byte[1024];
                        try {
                            bytes = inputStream.read(data);
                            if (bytes>0) {
                                handler.obtainMessage(STATE_ADDRESS_RECEIVED,bytes,-1,data).sendToTarget();
                            } else {
                                break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    } else if (messageType!=null && messageType.equals("int")) {
                        Log.i("peers", "message type: Initial");
                        data = new byte[1024];
                        try {
                            bytes = inputStream.read(data);
                            if (bytes>0) {
                                handler.obtainMessage(STATE_INIT_RECEIVED,bytes,-1,data).sendToTarget();
                            } else {
                                break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                    flag = true;
                }

            }
        }

        public void write(byte[] bytes) {
            try {
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void writeImage(byte[] bytes) {
            try {
                dataOutputStream.writeInt(bytes.length);
                dataOutputStream.write(bytes, 0, bytes.length);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        public void cancel() {
            try {
                if (socket != null) {
                    socket.close();
                    wfState = NOT_CONNECTED;
                }
            } catch (IOException e) {}
            //isClosed = true;
            //try { if (inputStream != null) inputStream.close(); } catch(IOException e) {}
            //try { if (dataInputStream != null) dataInputStream.close(); } catch(IOException e) {}
            //try { if (outputStream != null) outputStream.close(); } catch(IOException e) {}
            //try { if (dataOutputStream != null) dataOutputStream.close(); } catch(IOException e) {}
        }
    }

    public void writeBluetooth(byte[] msg) {
        try {
            Log.i("peers", "write bluetooth");
            bSendReceive.write(msg);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void writeBluetoothImg(byte[] img) {
        try {
            Log.i("peers", "write bluetooth image");
            bSendReceive.writeImage(img);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void writeWifi(byte[] msg) {
        Log.i("peers", "write Wifi");
        try {
            wClientClass.wSendReceive.write(msg);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void writeWiFiImg(byte[] img) {
        try {
            Log.i("peers", "write wifi image");
            wClientClass.wSendReceive.writeImage(img);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void closeMessenger() {
        /*
        if (wSendReceive!=null) {
            Log.i("peers", "close wSendReceive");
            wSendReceive.cancel();
            wSendReceive.interrupt();
        }
        */
        if (threadHandler!=null) {
            Log.i("peers", "close threadHandler");
            threadHandler.cancel();
        }
        if (wClientClass!=null) {
            Log.i("peers", "close wClientClass");
            wClientClass.cancel();
            wClientClass.interrupt();
        }
        /*
        if (wServerClass!=null) {
            Log.i("peers", "close wServerClass");
            wServerClass.cancel();
            wServerClass.interrupt();
        }
        */
    }

}
