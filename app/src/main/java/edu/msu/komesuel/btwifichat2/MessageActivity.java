package edu.msu.komesuel.btwifichat2;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class MessageActivity extends AppCompatActivity {

    /**
     * Messenger Class Instance
     */
    private Messenger messenger;

    /**
     * Bluetooth Variables
     */
    private BluetoothAdapter bluetoothAdapter;
    private Intent btEnablingIntent;
    private BluetoothDevice bluetoothDevice;
    private int requestCodeForEnable;
    private String[] btDeviceNameArray;
    private BluetoothDevice[] btDeviceArray;

    /**
     * WiFi Variables
     */
    private IntentFilter intentFilter;
    private WifiP2pManager.Channel channel;
    private WifiP2pManager p2pManager;
    private WifiManager wifiManager;
    private WifiP2pConfig config;
    private InetAddress address;
    private Collection<WifiP2pDevice> peers;
    private String[] wifiDeviceNameArray; // peer list listener
    private WifiP2pDevice[] wifiDeviceArray; // peer list listener
    private ArrayList<String> addressArray; // connected devices
    private String myAddressString; // this device address
    private String theirAddressString; // used when receiving connected device address
    private String hostAddressString; // store next group owner address
    private int state; // wifi state from messenger instance
    private String role; // host or client
    public static Queue<String> requestQueue = new LinkedList<>(); // simultaneous connection case

    /**
     * Boolean Variables
     */
    private boolean changingBool = false; // triggered when change group owner process begins
    private boolean threadStarted = false; // triggered when messenger side socket is opened
    private boolean switchBool = false; // triggered in changeHandler to alternate addition and subtraction of delay time

    private int order;


    /**
     * Spinner Cases
     */
    private String spinnerString;
    static final int BLUETOOTH = 1;
    static final int WIFI = 0;

    /**
     * Handler States
     */
    static final int NOT_CONNECTED = 0;
    static final int STATE_LISTENING = 1;
    static final int STATE_CONNECTING = 2;
    static final int STATE_CONNECTED_BT = 3;
    static final int STATE_CONNECTED_WF = 4;
    static final int STATE_CONNECTION_FAILED = 5;
    static final int STATE_MESSAGE_RECEIVED = 6;
    static final int STATE_IMAGE_RECEIVED = 7;
    static final int STATE_ADDRESS_RECEIVED = 8;
    static final int STATE_SERVER_STARTED = 9;
    static final int STATE_INIT_RECEIVED = 10;

    /**
     * Request code when selecting a picture
     */
    private static final int SELECT_PICTURE = 1;

    /**
     * The image bitmap. None initially.
     */
    private Bitmap imageBitmap = null;

    /**
     * Component Getters
     *
     */
    // Connection Status
    private TextView getConnectionStatus() { return (TextView) findViewById(R.id.connectionStatus); }
    // Spinner
    private Spinner getSpinner() { return (Spinner) findViewById(R.id.spinnerMethod); }
    // Edit Text
    public TextView getWriteMsg() { return (TextView) findViewById(R.id.editText); }
    // Message View
    private TextView getMessage() { return (TextView) findViewById(R.id.messageView); }
    // Image View
    private ImageView getImage() { return (ImageView) findViewById(R.id.imageView); }


    /**
     * ON CREATE
     *
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        startBluetooth(); // set up bluetooth
        startWiFi(); // set up WiFi
        messenger = new Messenger(connectHandler); // messenger instance

        /*
         * Set up the spinner
         * Choose with which method message is sent
         */
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.methods_spinner, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        getSpinner().setAdapter(adapter);

        getSpinner().setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int pos, long id) {
                switch (pos) {
                    case WIFI:
                        Message msg2 = Message.obtain();
                        msg2.what = messenger.getWfState();
                        connectHandler.sendMessage(msg2);
                        spinnerString = "Wifi";
                        break;
                    case BLUETOOTH:
                        Message msg = Message.obtain();
                        msg.what = messenger.getBtState();
                        connectHandler.sendMessage(msg);
                        spinnerString = "Bluetooth";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Message msg = Message.obtain();
                msg.what = NOT_CONNECTED;
                connectHandler.sendMessage(msg);
                spinnerString = "Wifi";
            }

        });

       /*
        * Permissions
        */
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},1);
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
        }

    }

    /**
     * MENU
     *
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_devices, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_bt:
                // Bluetooth Devices
                createBtDialog();
                return true;
            case R.id.menu_wifi:
                // WiFi Devices
                createWifiDialog();
                return true;
            case R.id.menu_go:
                // Change Group Owner
                createChooseHostDialog(); // choose new host
                return true;
            case R.id.menu_disconnect:
                // Disconnect Group
                changingBool = false;
                if (p2pManager!=null && channel!=null) {
                    p2pManager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener() {
                        @Override
                        public void onGroupInfoAvailable(WifiP2pGroup group) {
                            deletePersistentGroup(group);
                        }
                    });
                }
                disconnectGroup(false);
                changeHandler.removeCallbacks(changeRunnable); // end change group owner loop if running
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * STATE HANDLER
     *
     */
    Handler connectHandler = new Handler(new Handler.Callback() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case NOT_CONNECTED:
                    getConnectionStatus().setText("Not Connected");
                    break;
                case STATE_LISTENING:
                    getConnectionStatus().setText("Listening");
                    break;
                case STATE_CONNECTING:
                    getConnectionStatus().setText("Connecting");
                    break;
                case STATE_CONNECTED_BT:
                    getConnectionStatus().setText("Bluetooth Connected");
                    break;
                case STATE_CONNECTED_WF:
                    Log.i("peers", "wf connected");
                    changingBool = false;

                    if (role.equals("Client")) {
                        // Client side stop peer discovery once connected
                        p2pManager.stopPeerDiscovery(channel, new WifiP2pManager.ActionListener() {
                            @Override
                            public void onSuccess() {
                                Log.i("peers","wf connected stop peer discovery success");
                            }

                            @Override
                            public void onFailure(int reason) {
                                Log.i("peers","wf connected stop peer discovery failure: "+reason);
                            }
                        });
                        // Send Host your device address
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String temp = "int";
                                String msg=myAddressString;
                                messenger.writeWifi(temp.getBytes());
                                messenger.writeWifi(msg.getBytes());
                            }
                        });
                        thread.start();
                    }

                    getConnectionStatus().setText("WiFi Connected");

                    break;
                case STATE_CONNECTION_FAILED:
                    getConnectionStatus().setText("Connection Failed");
                    break;
                case STATE_MESSAGE_RECEIVED:
                    byte[] readBuff = (byte[]) msg.obj;
                    final String[] tempMsg = {new String(readBuff, 0, msg.arg1)};
                    getMessage().setText(tempMsg[0]);

                    if (role.equals("Host")) {
                        // Forward message to all clients
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                messenger.writeOnClientThreads("str".getBytes());
                                messenger.writeOnClientThreads(tempMsg[0].getBytes());
                            }
                        });
                        thread.start();
                    }

                    break;
                case STATE_IMAGE_RECEIVED:
                    final byte[] imgBuff = (byte[]) msg.obj;
                    Bitmap bitmap = BitmapFactory.decodeByteArray(imgBuff , 0, imgBuff.length);
                    getImage().setImageBitmap(bitmap);

                    if (role.equals("Host")) {
                        // Forward image to all clients
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                messenger.writeOnClientThreads("img".getBytes());
                                messenger.writeOnAllClientsImg(imgBuff);
                            }
                        });
                        thread.start();
                    }

                    break;
                case STATE_ADDRESS_RECEIVED:
                    /*
                     * Received new Host address
                     * Initiates Client side change group owner
                     */
                    byte[] buff = (byte[]) msg.obj;
                    hostAddressString = new String(buff, 0, msg.arg1);
                    Log.i("peers", "state address received: "+hostAddressString);

                    changingBool = true;
                    threadStarted = false;
                    messenger.changeWfState(NOT_CONNECTED);


                    if (hostAddressString.equals(myAddressString)) {
                        // New Host disconnects existing group and creates new one

                        addressArray = new ArrayList<>();

                        swapRoles();
                        changingBool = true;
                        threadStarted = false;


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                disconnectGroup(changingBool);
                                Log.i("peers", "disconnect group called");

                            }
                        }, 1000);

                    } else {
                        // Initiate change group owner loop with delay
                        changeHandler.postDelayed(changeRunnable, 5000);
                    }

                    break;
                case STATE_SERVER_STARTED:
                    threadStarted = true;
                    messenger.startWfServer();
                    break;
                case STATE_INIT_RECEIVED:
                    // Host side receives client address
                    byte[] buff1 = (byte[]) msg.obj;
                    theirAddressString = new String(buff1, 0, msg.arg1);
                    Log.i("peers", "client address received: "+theirAddressString);
                    addressArray.add(theirAddressString);
                    getConnectionStatus().setText("WiFi Connected");
                    break;
            }

            return true;
        }
    });

    /**
     * BLUETOOTH SETUP
     *
     */
    public void createBtDialog(){
        // start bt server on all devices so either can connect as client
        messenger.startBtServer();
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Bluetooth Device");
        builder.setItems(btDeviceNameArray, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                bluetoothDevice = btDeviceArray[which];
                messenger.startBtClient(bluetoothDevice);
                //getConnectionStatus().setText("Connecting");
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void startBluetooth() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        btEnablingIntent = new Intent(bluetoothAdapter.ACTION_REQUEST_ENABLE);
        requestCodeForEnable = 1;

        btDeviceNameArray = new String[bluetoothAdapter.getBondedDevices().size()];
        btDeviceArray = new BluetoothDevice[bluetoothAdapter.getBondedDevices().size()];
        int index = 0;
        Set<BluetoothDevice> bt = bluetoothAdapter.getBondedDevices();
        if (bt.size() > 0) {
            for (BluetoothDevice device : bt) {
                if (device.getName() != null) {
                    btDeviceArray[index] = device;
                    btDeviceNameArray[index] = device.getName();
                }
            }
        }
    }

    /**
     * WIFI SETUP
     *
     */
    public void createWifiDialog() {
        // setup the WiFi Devices Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Available Peers");
        try {
            builder.setItems(wifiDeviceNameArray, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    final WifiP2pDevice device = wifiDeviceArray[which];

                    config = new WifiP2pConfig();
                    config.deviceAddress = device.deviceAddress;

                    Log.i("peers", "createWifiDialog device address: "+device.deviceAddress);

                    if (!threadStarted) {
                        p2pManager.connect(channel, config, new WifiP2pManager.ActionListener() {
                            @Override
                            public void onSuccess() {
                                Log.i("peers", "createWiFiDialog .connect on success");
                                p2pManager.requestConnectionInfo(channel,connectionInfoListener);
                                //threadStarted = true;
                                Toast.makeText(getApplicationContext(),
                                        "Connecting to "+device.deviceName,Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(int reason) {
                                Toast.makeText(getApplicationContext(), "Not Connected",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    }


                }
            });

            // Discover button restarts peer discovery
            builder.setPositiveButton("Discover", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    p2pManager.cancelConnect(channel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.i("peers", "wifi dialog discover cancel connect");

                        }

                        @Override
                        public void onFailure(int reason) {
                            Log.i("peers", "wifi dialog discover cancel connect failure: "+reason );
                        }
                    });
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        p2pManager.stopPeerDiscovery(channel, new WifiP2pManager.ActionListener() {
                            @Override
                            public void onSuccess() {
                                Log.i("peers", "peer discovery stopped");
                            }

                            @Override
                            public void onFailure(int reason) {
                                Log.i("peers", "peer discovery stop failure");
                            }
                        });
                    }
                    p2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(getApplicationContext(),"WiFi Discovery Restarted",Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(int reason) {
                            Toast.makeText(getApplicationContext(),"WiFi Discovery Failed - "+reason ,Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void startWiFi() {
        intentFilter = new IntentFilter();

        // Provides API for managing WiFi peer2peer connectivity
        p2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        // Channel that connects application to WiFi peer2peer network
        channel = p2pManager.initialize(this, getMainLooper(), null);
        // Provides API for managing WiFi connectivity
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        // Initialize address array
        addressArray = new ArrayList<>();

        // Start peer discovery
        p2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getApplicationContext(),"WiFi Discovery Started",Toast.LENGTH_LONG).show();
                Log.i("peers", "Discovery Started");
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(getApplicationContext(),"WiFi Discovery Failed",Toast.LENGTH_LONG).show();
                Log.i("peers", "Discovery Failed");
            }
        });


        // Indicates a change in the Wi-Fi P2P status.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        // Indicates a change in the list of available peers.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        // Indicates the state of Wi-Fi P2P connectivity has changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        // Indicates this device's details have changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

    }

    /**
     * PEER LIST LISTENER
     *
     */
    private WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {
            // Update discovered peers  array list for WiFi Devices dialog
            peers = new ArrayList<WifiP2pDevice>();
            Collection<WifiP2pDevice> refreshedPeers = peerList.getDeviceList();
            if (!refreshedPeers.equals(peers)) {
                peers.clear();
                peers.addAll(refreshedPeers);

                // Store refreshed peer information in respective arrays
                wifiDeviceNameArray = new String[refreshedPeers.size()];
                wifiDeviceArray = new WifiP2pDevice[refreshedPeers.size()];

                int index = 0;
                for (WifiP2pDevice device : refreshedPeers) {
                    wifiDeviceNameArray[index] = device.deviceName;
                    wifiDeviceArray[index] = device;
                    index++;
                }
            }

            if (peers.size() == 0) {
                Toast.makeText(getApplicationContext(),"No Device Found",Toast.LENGTH_SHORT).show();
                return;
            }

        }
    };

    /**
     * CONNECTION INFO LISTENER
     *
     */
    WifiP2pManager.ConnectionInfoListener connectionInfoListener = new WifiP2pManager.ConnectionInfoListener() {
        @Override
        public void onConnectionInfoAvailable(WifiP2pInfo info) {
            Log.i("peers", "group formed: "+info.groupFormed );
            Log.i("peers", "is group owner: "+info.isGroupOwner );
            Log.i("peers", "group owner: "+info.groupOwnerAddress);

            // Get current WiFi state
            state = messenger.getWfState();

            if (info.groupFormed) {
                // When device is group owner is not connected
                if (info.isGroupOwner && role != null && role.equals("Client")) {
                    p2pManager.removeGroup(channel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.i("peers", " groupOwner && client remove group success");
                        }

                        @Override
                        public void onFailure(int reason) {
                            Log.i("peers", "groupOwner && client remove group failure: "+ reason);
                        }
                    });
                }else if (info.isGroupOwner && state!=STATE_CONNECTED_WF) {
                    role = "Host";
                    //START SERVER SIDE
                    if (!threadStarted) {
                        // Thread Handler keeps array list of server class instances for each client
                        messenger.startThreadHandler();

                        // Start peer discovery
                        p2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                            @Override
                            public void onSuccess() {
                                Log.i("peers", "cil server discover peers success");
                            }

                            @Override
                            public void onFailure(int reason) {
                                Log.i("peers", "cil server discover peers failure: "+reason);
                            }
                        });

                        Toast.makeText(getApplicationContext(),"Host",Toast.LENGTH_SHORT).show();
                        Log.i("peers", "server");
                    }
                } else if (state!=STATE_CONNECTED_WF) {
                    //START CLIENT SIDE
                    role = "Client";
                    changingBool = false;
                    messenger.startWfClient(info.groupOwnerAddress);

                    Toast.makeText(getApplicationContext(),"Client",Toast.LENGTH_SHORT).show();
                } else if (info.isGroupOwner){
                    // Handle Simultaneous Connections
                    Log.i("peers", "create new connections");
                    if (requestQueue.isEmpty()) {
                        Log.i("peers", "queue is empty");
                        messenger.startWfServer();
                    } else {
                        Log.i("peers", "queue not empty");
                        requestQueue.add(myAddressString);
                        simultaneousHandler.postDelayed(simultaneousRunnable,3000);
                    }

                }
            }

        }
    };

    /**
     * BROADCAST RECEIVER
     *
     */
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                // Determine if Wifi P2P mode is enabled or not, alert
                // the Activity.
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                    Toast.makeText(context, "WiFi P2P is on",Toast.LENGTH_SHORT).show();
                    //activity.setIsWifiP2pEnabled(true);
                } else {
                    Toast.makeText(context, "WiFi P2P is off",Toast.LENGTH_SHORT).show();
                    //activity.setIsWifiP2pEnabled(false);
                }
            } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                // Request available peers from the wifi p2p manager. This is an
                // asynchronous call and the calling activity is notified with a
                // callback on PeerListListener.onPeersAvailable()
                if (p2pManager != null) {
                    p2pManager.requestPeers(channel, peerListListener);
                }
                Toast.makeText(getApplicationContext(), "Peers Changed",
                        Toast.LENGTH_SHORT).show();
                //Log.i("peers", "P2P peers changed");
            } else if (WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION.equals(intent.getAction())) {
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_DISCOVERY_STATE, 10000);
                if (state == WifiP2pManager.WIFI_P2P_DISCOVERY_STARTED)
                {
                    Log.i("peers", "br discovery started");
                }
                else
                {
                    Log.i("peers", "br discovery stopped");
                    // Wifi P2P discovery stopped.
                    // Do what you want to do when discovery stopped
                }
            } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                if (p2pManager == null) { return; }
                NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                Log.i("peers", "WIFI_P2P_CONNECTION_CHANGED_ACTION");
                Log.i("peers", "network: "+ networkInfo.isConnected());
                Log.i("peers", "role: "+role);
                Log.i("peers", "changingBool: "+changingBool);
                if (networkInfo.isConnected()){
                    Log.i("peers", "network is connected");
                    p2pManager.requestConnectionInfo(channel,connectionInfoListener);
                } else if (role==null) {
                    Log.i("peers", "Device Disconnected");
                    getConnectionStatus().setText("Device Disconnected");
                } else if (role.equals("Client") && !changingBool && p2pManager!= null && channel!=null){
                    Log.i("peers", "!changingBool && p2pManager!= null && channel!=null");
                    Log.i("peers", "role: "+role);

                    changeHandler.removeCallbacks(changeRunnable);
                    changeHandler.postDelayed(changeRunnable, 0);

                }
            } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                WifiP2pDevice device = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);

                myAddressString = device.deviceAddress;

                Log.d("peers", "my address (BR): " + myAddressString);
            }
        }
    };

    /** register the BroadcastReceiver with the intent values to be matched */
    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @RequiresApi(api = Build.VERSION_CODES.O_MR1)
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (p2pManager!=null && channel!=null) {
            p2pManager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener() {
                @Override
                public void onGroupInfoAvailable(WifiP2pGroup group) {
                    deletePersistentGroup(group);
                }
            });
            messenger.closeMessenger();
        }
        channel.close();

    }


    /**
     * Function called when we get a result from some external
     * activity called with startActivityForResult()
     * @param requestCode the request code we sent to the activity
     * @param resultCode a result of from the activity - ok or cancelled
     * @param data data from the activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_PICTURE && resultCode == Activity.RESULT_OK) {
            // Response from the picture selection activity
            Uri imageUri = data.getData();

            // We have to query the database to determine the document ID for the image
            Cursor cursor = getContentResolver().query(imageUri, null, null, null, null);
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":")+1);
            cursor.close();

            // Next, we query the content provider to find the path for this
            // document id.
            cursor = getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();

            if(path != null) {
                Log.i("peers", path);
                imageBitmap = BitmapFactory.decodeFile(path);
                try {
                    ExifInterface exif = new ExifInterface(path);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    Log.d("EXIF", "Exif: " + orientation);
                    Matrix matrix = new Matrix();
                    if ( orientation == ExifInterface.ORIENTATION_ROTATE_90 ) {
                        matrix.postRotate(90);
                    }
                    else if ( orientation == ExifInterface.ORIENTATION_ROTATE_180 ) {
                        matrix.postRotate(180);
                    }
                    else if ( orientation == ExifInterface.ORIENTATION_ROTATE_270 ) {
                        matrix.postRotate(270);
                    }
                    // rotating bitmap
                    imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0,
                            imageBitmap.getWidth(), imageBitmap.getHeight(), matrix, true);
                }
                catch (Exception e) {

                }
                getImage().setImageBitmap(imageBitmap);
            }

        }

    }

    /**
     * Handle a Picture button press
     * @param view
     */
    public void onPicture(View view) {
        // Get a picture from the gallery
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    /**
     * Handle a Clear button press
     * @param view
     */
    public void onClear(View view) {
        // Clear image view
        if (imageBitmap != null) {
            getImage().setImageResource(android.R.color.transparent);
            imageBitmap = null;
        }
    }

    /**
     * Handle a Send button press
     * @param view
     */
    public void onSend(View view) {
        Log.i("peers", spinnerString);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    if (imageBitmap != null) {
                        // Send Image
                        String temp = "img";
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        imageBitmap.compress(Bitmap.CompressFormat.JPEG,25,outputStream);
                        byte[] imageBytes = outputStream.toByteArray();
                        if (spinnerString.equals("Bluetooth")) {
                            messenger.writeBluetooth(temp.getBytes());
                            Log.i("peers", "image BT before write");
                            messenger.writeBluetoothImg(imageBytes);
                        } else {
                            if (role.equals("Host")) {
                                messenger.writeOnAllThreads(temp.getBytes());
                                Log.i("peers", "image WF before write");
                                messenger.writeOnAllThreadsImg(imageBytes);
                            } else {
                                messenger.writeWifi(temp.getBytes());
                                Log.i("peers", "image WF before write");
                                messenger.writeWiFiImg(imageBytes);
                            }

                        }
                    } else {
                        // Send Message
                        String temp = "str";
                        String msg=getWriteMsg().getText().toString();
                        if (spinnerString.equals("Bluetooth")) {
                            messenger.writeBluetooth(temp.getBytes());
                            msg = "B: "+msg;
                            messenger.writeBluetooth(msg.getBytes());
                        } else {
                            if (role.equals("Host")) {
                                messenger.writeOnAllThreads(temp.getBytes());
                                msg = "W: "+msg;
                                messenger.writeOnAllThreads(msg.getBytes());
                            } else {
                                messenger.writeWifi(temp.getBytes());
                                msg = "W: "+msg;
                                messenger.writeWifi(msg.getBytes());
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.i("peers", "send failure");
                    e.printStackTrace();
                }
            }
        });
        thread.start();

    }

    /**
     * WIFI P2P GROUP CHANGE
     */

    /**
     * DISCONNECT GROUP
     * Sever all active group connections
     */
    public void disconnectGroup(final boolean change) {
        if (p2pManager != null && channel != null) {
            p2pManager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener() {
                @Override
                public void onGroupInfoAvailable(final WifiP2pGroup group) {
                    if (group != null && p2pManager != null && channel != null) {
                        p2pManager.removeGroup(channel, new WifiP2pManager.ActionListener() {
                            @Override
                            public void onSuccess() {
                                Log.i("peers", "removeGroup onSuccess");
                                messenger.closeMessenger();
                                deletePersistentGroup(group);
                            }

                            @Override
                            public void onFailure(int reason) {
                                Log.i("peers", "removeGroup onFailure -" + reason);
                            }
                        });
                    }
                }
            });
            Log.i("peers", "changingBool: "+changingBool);


        }

    }

    /**
     * DELETE PERSISTENT GROUP
     * Hidden method delete persistent group
     */
    private void deletePersistentGroup(WifiP2pGroup wifiP2pGroup) {
        try {
            Method getNetworkId = WifiP2pGroup.class.getMethod("getNetworkId");
            Integer networkId = (Integer) getNetworkId.invoke(wifiP2pGroup);
            Method deletePersistentGroup = WifiP2pManager.class.getMethod("deletePersistentGroup",
                    WifiP2pManager.Channel.class, int.class, WifiP2pManager.ActionListener.class);
            deletePersistentGroup.invoke(p2pManager, channel, networkId, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.e("peers", "deletePersistentGroup onSuccess");
                    //disconnectGroup(changingBool);
                    if (changingBool) changeGroupOwner();
                }

                @Override
                public void onFailure(int reason) {
                    Log.e("peers", "deletePersistentGroup failure: " + reason);
                }
            });
        } catch (NoSuchMethodException e) {
            Log.i("peers", "Could not delete persistent group", e);
        } catch (InvocationTargetException e) {
            Log.i("peers", "Could not delete persistent group", e);
        } catch (IllegalAccessException e) {
            Log.i("peers", "Could not delete persistent group", e);
        } catch (NullPointerException e) {
            Log.i("peers", "Could not delete persistent group", e);
        }
    }

    /**
     * Swap roles
     * Host becomes Client
     * Client becomes Host
     */
    public void swapRoles() {
        Log.i("peers", "swapRoles");
        if (role.equals("Host")) {
            role = "Client";
            Log.i("peers", "was Host now Client");

            p2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.i("peers", "swaproles discover peers on success");
                }

                @Override
                public void onFailure(int reason) {
                    Log.i("peers", "swaproles discover peers on failure: "+reason);
                }
            });

        } else if (role.equals("Client")) {
            role = "Host";
            Log.i("peers", "was Client now Host");
        }

    }

    /**
     * CHOOSE NEW HOST DIALOG
     * Creates Dialog of client addresses
     *
     */

    public void createChooseHostDialog() {

        final String[] clientAddresses = new String[addressArray.size()];

        Log.i("peers", "createChooseHostDialog addressArray.size() "+addressArray.size());

        int i = 0;
        for (String address : addressArray) {
            clientAddresses[i] = address;
            i++;
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose New Host");
        builder.setItems(clientAddresses, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                hostAddressString = clientAddresses[which];
                if (hostAddressString != null) {
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("peers", "cgo on press hostadd: "+hostAddressString);
                            String temp = "add";
                            String msg=hostAddressString;
                            messenger.writeOnAllThreads(temp.getBytes());
                            messenger.writeOnAllThreads(msg.getBytes());
                        }
                    });
                    thread.start();
                    swapRoles();
                    changingBool = true;
                    threadStarted = false;

                    messenger.changeWfState(NOT_CONNECTED);

                    changeHandler.postDelayed(changeRunnable, 8000);

                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();







    }


    /**
     * CHANGE GROUP OWNER
     * Host: create new group
     * Client: attempt connection
     *
     */
    public void changeGroupOwner() {
        Log.i("peers", "changeGroupOwners");

        p2pManager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener() {
            @Override
            public void onGroupInfoAvailable(WifiP2pGroup group) {
                if (group!=null) {
                    Log.i("peers", "group available: "+group.isGroupOwner());
                    deletePersistentGroup(group);
                } else {
                    Log.i("peers", "group doesn't exist");
                }

            }
        });

        if (role.equals("Host")) {
            changingBool = false;
            Log.i("peers", "change GO Host");
            p2pManager.createGroup(channel, new WifiP2pManager.ActionListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onSuccess() {
                    Log.i("peers", "Create group successful");
                    p2pManager.stopPeerDiscovery(channel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            messenger.changeWfState(NOT_CONNECTED);

                            p2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                                @Override
                                public void onSuccess() {

                                    Log.i("peers", "cgo host discover peers on success");
                                }

                                @Override
                                public void onFailure(int reason) {
                                    Log.i("peers", "cgo host discover peers on failure: "+reason);
                                }
                            });

                        }

                        @Override
                        public void onFailure(int reason) {
                            Log.i("peers", "cgo host stop discover on failure: "+reason);
                            //changeGroupOwner();
                        }
                    });
                }
                @Override
                public void onFailure(int reason) {
                    Log.i("peers", "Create group failed: "+reason);
                }
            });
        } else if (role.equals("Client")) {

            config = new WifiP2pConfig();
            config.deviceAddress = hostAddressString;
            p2pManager.connect(channel, config, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.i("peers", "change group owner .connect on success");
                    messenger.changeWfState(NOT_CONNECTED);

                    p2pManager.requestConnectionInfo(channel,connectionInfoListener);
                    Toast.makeText(getApplicationContext(),
                            "Connecting",Toast.LENGTH_SHORT).show();
                    changingBool = false;
                }

                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onFailure(int reason) {
                    Log.i("peers", "connect On Failure: "+reason);
                    Toast.makeText(getApplicationContext(), "Connection Failed",
                            Toast.LENGTH_SHORT).show();
                    Message temp = Message.obtain();
                    temp.what = STATE_CONNECTION_FAILED;
                    connectHandler.sendMessage(temp);

                    changingBool = true;

                    p2pManager.cancelConnect(channel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.i("peers", "change handler cancel connect success");
                        }

                        @Override
                        public void onFailure(int reason) {
                            Log.i("peers", "change handler cancel connect failure: "+reason);
                        }
                    });

                    p2pManager.stopPeerDiscovery(channel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.i("peers", "change handler stop peer discovery success");
                        }

                        @Override
                        public void onFailure(int reason) {
                            Log.i("peers", "change handler stop peer discovery failure: "+reason);
                        }
                    });


                    p2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.i("peers", "change handler discover peers restarted");
                        }

                        @Override
                        public void onFailure(int reason) {
                            Log.i("peers", "change handler rediscover peers failure: "+reason);
                        }
                    });


                }
            });


        }
    }

    /**
     * Handle Change Group Owner Client Side
     * Continue to loop until success
     *
     */
    private Handler changeHandler = new Handler();
    private Runnable changeRunnable = new Runnable() {
        @Override
        public void run() {
            final int state = messenger.getWfState();
            int delay = 5000;


            Log.i("peers", "change handler: "+hostAddressString);
            Log.i("peers", "change handler: "+role);
            Log.i("peers", "wf state: "+state);

            if (state!=STATE_CONNECTED_WF) {
                p2pManager.requestGroupInfo(channel, new WifiP2pManager.GroupInfoListener() {
                    @Override
                    public void onGroupInfoAvailable(WifiP2pGroup group) {
                        if (group!=null) {
                            Log.i("peers", "group still exists");
                            p2pManager.removeGroup(channel, new WifiP2pManager.ActionListener() {
                                @Override
                                public void onSuccess() {
                                    Log.i("peers", "changeHandler remove group success");
                                    changeGroupOwner();
                                }

                                @Override
                                public void onFailure(int reason) {
                                    Log.i("peers", "changeHandler remove group failed");

                                }
                            });
                        } else {
                            Log.i("peers", "group doesn't exist");
                            changeGroupOwner();
                        }
                    }
                });

                // CHANGE GROUP OWNER CLIENT CODE WAS HERE

                delay = (switchBool) ? delay+1000 : delay-1000;
                switchBool = !switchBool;
                Log.i("peers", "delay: "+delay);
                changeHandler.postDelayed(changeRunnable,delay);
            } else {
                Log.i("peers", "remove callbacks");
                changeHandler.removeCallbacks(changeRunnable);
            }

        }
    };


    /**
     * HANDLE SIMULTANEOUS SERVER SIDE CONNECTIONS
     * Go in order of queue
     */
    private Handler simultaneousHandler = new Handler();
    private Runnable simultaneousRunnable = new Runnable() {
        @Override
        public void run() {
            Log.i("peers", "in simultaneousRunnable");
            if (requestQueue.remove().equals(myAddressString)) {
                Log.i("peers", "head of queue, starting server");
                messenger.startWfServer();
            } else {
                Log.i("peers", "not head, loop simultaneous");
                simultaneousHandler.postDelayed(simultaneousRunnable,3000);
            }

        }


    };



}
